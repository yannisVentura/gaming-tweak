#  Pour lancer un script PowerShell en tant qu'administrateur Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
Start-Process powershell -Verb runAs
Write-Host "Save old default configuration to $HOME\Documents\My Games\old ..."
New-Item -ItemType Directory -Force -Path "$HOME\Documents\My Games\Fallout4\old"
Copy-Item "$HOME\Documents\My Games\Fallout4\Fallout4Custom.ini" "$HOME\Documents\My Games\Fallout4\old\Fallout4Custom.ini"
Copy-Item "$HOME\Documents\My Games\Fallout4\Fallout4.ini" "$HOME\Documents\My Games\Fallout4\old\Fallout4.ini"
Copy-Item "$HOME\Documents\My Games\Fallout4\Fallout4Prefs.ini" "$HOME\Documents\My Games\Fallout4\old\Fallout4Prefs.ini"

write-host "Copy custom tweak to Documents\My Games\Fallout4..."
Copy-Item .\Fallout4.ini "$HOME\Documents\My Games\Fallout4"
Copy-Item .\Fallout4Custom.ini "$HOME\Documents\My Games\Fallout4"
Copy-Item .\Fallout4Prefs.ini "$HOME\Documents\My Games\"

Write-Host "Set read-only to custom files"
Set-ItemProperty -Path "$HOME\Documents\My Games\Fallout4\Fallout4Custom.ini" -Name IsReadOnly -Value $true
Set-ItemProperty -Path "$HOME\Documents\My Games\Fallout4\Fallout4.ini" -Name IsReadOnly -Value $true
Set-ItemProperty -Path "$HOME\Documents\My Games\Fallout4\Fallout4Prefs.ini" -Name IsReadOnly -Value $true
Write-Host "End..."