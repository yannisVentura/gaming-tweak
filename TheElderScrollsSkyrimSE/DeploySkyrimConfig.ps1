Get-ChildItem -Path "$HOME\Documents\My Games" -Recurse -File | % { $_.IsReadOnly=$False}

Write-Host "Save old default configuration to $HOME\Documents\My Games\old ..."
New-Item -ItemType Directory -Force -Path "$HOME\Documents\My Games\Skyrim Special Edition\old"

# Besoin de vérification du path sinon force copy
Move-Item "$HOME\Documents\My Games\Skyrim Special Edition\Skyrim.ini" "$HOME\Documents\My Games\Skyrim Special Edition\old\Skyrim.ini"
Move-Item "$HOME\Documents\My Games\Skyrim Special Edition\SkyrimPrefs.ini" "$HOME\Documents\My Games\Skyrim Special Edition\old\SkyrimPrefs.ini"

write-host "Copy custom tweak to Documents\My Games\Skyrim Special Edition..."
Copy-Item .\Skyrim.ini "$HOME\Documents\My Games\Skyrim Special Edition"
Copy-Item .\SkyrimPrefs.ini "$HOME\Documents\My Games\Skyrim Special Edition"

Write-Host "Set read-only to custom files"
Set-ItemProperty -Path "$HOME\Documents\My Games\Skyrim Special Edition\Skyrim.ini" -Name IsReadOnly -Value $true
Set-ItemProperty -Path "$HOME\Documents\My Games\Skyrim Special Edition\SkyrimPrefs.ini" -Name IsReadOnly -Value $true

Get-ChildItem -Path "$HOME\Documents\My Games" -Recurse -File | % { $_.IsReadOnly=$True}
Write-Host "End..."